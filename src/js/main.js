$(document).ready(function(){
	$('.main-slider').slick({
		dots: true,
		autoplay: true,
		autoplaySpeed: 3000,
		pauseOnHover: false
	});
	$('.feed-slider').slick({
		prevArrow: '<button type="button" class="slick-prev slick-arrow--chevron">Previous</button>',
		nextArrow: '<button type="button" class="slick-next slick-arrow--chevron">Next</button>'
	});
	$('.tilt-box').tilt();
});
