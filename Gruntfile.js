module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				files: {
					'public/css/style.css' : 'src/css/style.scss'
				}
			}
		},
		uglify: {
			src: {
				files: {
					'public/js/main.min.js': ['src/js/main.js']
				}
			}
		},
		postcss: {
			options: {
				map: true,
				processors: [
					require('autoprefixer')
				]
			},
			dist: {
				src: 'public/css/style.css'
			}
		},
		watch: {
			css: {
				files: 'src/**/*.scss',
				tasks: ['sass', 'postcss']
			},
			js: {
				files: 'src/**/*.js',
				tasks: ['uglify']
			}
		},
		pug: {
			compile: {
				options: {
					data: {
						debug: false
					}
				},
				files: {
					'public/index.html': 'views/index.pug'
				}
			}
		}
	});
	grunt.loadNpmTasks('grunt-postcss');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default',['watch']);

	grunt.loadNpmTasks('grunt-contrib-pug');
	grunt.registerTask('compile',['pug']);
}
