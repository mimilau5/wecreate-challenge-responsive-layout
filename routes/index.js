var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'weCreate Challenge by Mimi Lau' });
});

module.exports = router;
