# weCreate Challenge

## Usage

### To install modules:
```
npm install
```

### To run Express app:
```
npm start
```
App will be running on http://localhost:3000/

### To edit files
Watch `.scss`, `.js` files, and build to `public/` folder
```
grunt watch
```

### To export HTML file
```
grunt compile
```
That will compile `views/index.pug` file to `public/index.html`

## Technology

* Framework: Express
* Template engine: Pug
* CSS framework : BootStrap-v4.3 (sass)
* Editor: Atom
